<?php require_once '../app/views/inc/header.php' ?>

<div class="jumbotron">
    <h1 class="display-4 text-center">OxyBlogger</h1>
    <p class="lead text-center">A simple Blog built on <b>Oxygen</b> MVC FRAMEWORK written in PHP</p>
    <hr class="my-4 text-center">
    <p class="text-center">Please read the docs to know more about the framework and how to get a copy of it.
        Development is still going on and contributions are allowed at the moment. Below is the link
        to the github repository for the framework
    </p>
    <p class="lead text-center">
        <a class="btn btn-dark btn-lg" href="#" role="button">Visit Github</a>
    </p>
</div>
<?php require_once '../app/views/inc/footer.php' ?>