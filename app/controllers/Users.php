<?php

class Users extends Controller
{

    private $userModel;
    public function __construct()
    {
        $this->userModel = $this->model('User');
    }

    public function register()
    {
        //Check for POST

        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            //Proccess Form
            $data = [
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
            ];

        
            

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            //Validate name
            if(empty($data['name']))
            {
                $this->error = true;
                $data['name_err'] = "Please enter your name";
            }

            //Validate Email

            if(empty($_POST['email']))
            {
                $this->error = true;
                $data['email_err'] = "Please enter your email";
            }else{
                if($this->userModel->findUserByEmail($data['email']))
                {
                    $this->error = true;
                    $data['email_err'] = "Email has already been taken";
                }
            }

            //Validate Password field
            if(empty($data['password']))
            {
                $this->error = true;
                $data['password_err'] = "Please enter your password";
            }elseif(strlen($data['password']) < 6)
            {
                $this->error = true;
                $data['password_err'] = "Your password cannot be less than 6 characters";
            }

            //Validate Confirm password field
            if(empty($data['confirm_password']))
            {
                $this->error = true;
                $data['confirm_password_err'] = "Please confirm your password";
            }elseif($data['confirm_password'] != $data['password'])
            {
                $this->error = true;
                $data['confirm_password_err'] = "Password does not match";
            }

            //Check if there is any error

            if($this->error == true)
            {
                //Error is present
                $this->view('auth/register', $data);
            }else{
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                if($this->userModel->register($data))
                {
                    flash('register_success', 'You are registered and can log in');
                    redirect('users/login');
                }
            }
            

            

        }else{
            //Load Form
            $data = [
                'name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
            ];

            //Load view
            $this->view('auth/register', $data);
        }
    }

    public function login()
    {
        //Check for POST

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            ];

            if (empty($_POST['email'])) {
                $this->error = true;
                $data['email_err'] = "Please enter your email";
            }else{
                if(!$this->userModel->findUserByEmail($data['email']))
                {
                    $this->error = true;
                    $data['email_err'] = "Incorrect Email";
                }
            }

            if (empty($data['password'])) {
                $this->error = true;
                $data['password_err'] = "Please enter your password";
            }

            if ($this->error == true) {
                //Error is present
                $this->view('auth/login', $data);
            } else {
                $loggedInUser = $this->userModel->login($data['email'], $data['password']);

                if($loggedInUser)
                {
                    $this->createUserSession($loggedInUser);
                }else{
                    $data["password_err"] = 'incorrect password';
                    $this->view('auth/login', $data);
                }
            }
        } else {
            //Load Form
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => '',
            ];

            //Load view
            $this->view('auth/login', $data);
        }
    }

    public function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;
        redirect('pages/index');
    }

    public function logout()
    {
        unset($_SESSION['user_id'] );
        unset($_SESSION['user_email'] );
        unset($_SESSION['user_name'] );
        session_destroy();
        redirect('pages/index');
    }

    public function isLoggedIn()
    {
        if(isset($_SESSION['user_id']))
        {
            return true;
        }else{
            return false;
        }
    }
}