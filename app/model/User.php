<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function findUserByEmail($email)
    {
        $sql = /** @lang  mysql */
            'SELECT * FROM users where email=:email';
        $this->db->query($sql);
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        if($this->db->rowCount() > 0)
        {
            return true;
        }else{
            return false;
        }
    }

    public function register($data)
    {
        $sql = /** @lang text */
            'INSERT INTO users(name, email, password) VALUES(:name, :email, :password)';

        $this->db->query($sql);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':password', $data['password']);


        if($this->db->execute())
        {
            return true;
        }else{
            return false;
        }
    }

    public function login($email, $password)
    {
        if($this->findUserByEmail($email))
        {
            $sql = /** @lang mysql */
                'SELECT * FROM users where email=:email';
            $this->db->query($sql);
            $this->db->bind(':email', $email);
            $row = $this->db->single();
            $hashed_password = $row->password;

            if(password_verify($password, $hashed_password))
            {
                return $row;
            }else{
                return false;
            }
        }
    }
}